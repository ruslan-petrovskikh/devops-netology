# devops-netology
test1

##### .gitignore #####                                                                                                                                   
**/.terraform/*         # Все каталоги .terraform с любым уровнем вложенности и содержимым                                                               
                                                                                                                                                         
*.tfstate               # Файлы с расширением .tfstate                                                                                                   
*.tfstate.*             # Файлы, имеющие в своём названии .tfstate.                                                                                      
                                                                                                                                                         
crash.log               # Файлы crash.log                                                                                                                
                                                                                                                                                         
*.tfvars                # Файлы с расширением .tfvars                                                                                                    
                                                                                                                                                         
override.tf             # Файлы override.tf                                                                                                              
override.tf.json        # Файлы override.tf.json                                                                                                         
*_override.tf           # Файлы с окончанием _override.tf                                                                                                
*_override.tf.json      # Файлы с окончанием _override.tf.json                                                                                           
                                                                                                                                                         
.terraformrc            # Файлы .terraformrc                                                                                                             
terraform.rc            # Файлы terraform.rc 

